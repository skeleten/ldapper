//! Error types and conversions

use std::os::raw::c_int;

/// Error type returned through out the library
#[derive(Debug)]
pub enum Error {
    IoError {
        inner: std::io::Error,
    },
    /// An error originating from `libldap`
    LdapError {
        /// LDAP error code
        errcode: c_int,
        /// Optional message received from [`ldap_sys::ldap_parse_result`]
        msg: Option<String>,
    },
    /// Error originating from a poisoned lock.
    /// See also: [`std::sync::PoisonError`]
    PoisonError, // TODO: Inner?
    /// Failure to convert bytes from/to UTF8
    Utf8Error {
        inner: std::str::Utf8Error,
    },
    FromUtf8Error {
        inner: std::string::FromUtf8Error,
    },
    NulError {
        inner: std::ffi::NulError,
    },
    /// A timeout was encountered
    Timeout,
    /// No message was received
    NoMessage,
}

impl Error {
    /// Check if the `Error` is of the `Timeout` variant
    pub fn is_timeout(&self) -> bool {
        matches!(self, Self::Timeout)
    }
}

impl From<std::io::Error> for Error {
    fn from(e: std::io::Error) -> Self {
        Error::IoError { inner: e }
    }
}

impl<T> From<std::sync::PoisonError<T>> for Error {
    fn from(_e: std::sync::PoisonError<T>) -> Self {
        Error::PoisonError
    }
}

impl From<std::str::Utf8Error> for Error {
    fn from(e: std::str::Utf8Error) -> Self {
        Error::Utf8Error { inner: e }
    }
}

impl From<std::string::FromUtf8Error> for Error {
    fn from(e: std::string::FromUtf8Error) -> Self {
        Error::FromUtf8Error { inner: e }
    }
}

impl From<std::ffi::NulError> for Error {
    fn from(e: std::ffi::NulError) -> Self {
        Error::NulError { inner: e }
    }
}
