use std::convert::TryFrom;
use std::ffi::{c_void, CString};
use std::os::raw::c_char;

use crate::{Error, Result};

/// Free the pointer `ptr` using `ldap_memfree` if it is not null
pub fn _ldap_memfree<T>(ptr: *mut T) {
    if !ptr.is_null() {
        unsafe {
            ldap_sys::ldap_memfree(ptr as *mut c_void);
        }
    }
}

/// Wraps a `*mut T` pointer and cleans it up using the provided
/// method when it exits the scope
pub struct ForeignMutPtr<T, F: FnOnce(*mut T)> {
    inner: *mut T,
    free: Option<F>,
}

impl<T, F> ForeignMutPtr<T, F>
where
    F: FnOnce(*mut T),
{
    pub fn new(val: *mut T, free_func: F) -> Self {
        ForeignMutPtr {
            inner: val,
            free: Some(free_func),
        }
    }
}

impl<T, F> std::ops::Drop for ForeignMutPtr<T, F>
where
    F: FnOnce(*mut T),
{
    fn drop(&mut self) {
        let ptr = std::mem::replace(&mut self.inner, std::ptr::null_mut());
        if !ptr.is_null() {
            let func = self.free.take().unwrap();
            func(ptr);
        }
    }
}

impl<T, F> AsPtr for ForeignMutPtr<T, F>
where
    F: FnOnce(*mut T),
{
    type Pointer = T;

    fn as_ptr(&self) -> *const T {
        self.inner
    }
}

impl<T, F> AsMutPtr for ForeignMutPtr<T, F>
where
    F: FnOnce(*mut T),
{
    type Pointer = <Self as AsPtr>::Pointer;

    fn as_mut_ptr(&mut self) -> *mut T {
        self.inner
    }
}

/// Wrapper around `CString` so we dont have to worry about cleaning those up manually
pub struct FfiString {
    inner: *mut c_char,
}

impl From<CString> for FfiString {
    fn from(s: CString) -> Self {
        FfiString {
            inner: s.into_raw(),
        }
    }
}

impl AsPtr for FfiString {
    type Pointer = c_char;

    fn as_ptr(&self) -> *const <Self as AsPtr>::Pointer {
        self.inner
    }
}

impl AsMutPtr for FfiString {
    type Pointer = <Self as AsPtr>::Pointer;

    fn as_mut_ptr(&mut self) -> *mut <Self as AsMutPtr>::Pointer {
        self.inner
    }
}

impl std::ops::Drop for FfiString {
    fn drop(&mut self) {
        let ptr = std::mem::replace(&mut self.inner, std::ptr::null_mut());
        if !ptr.is_null() {
            std::mem::drop(unsafe { CString::from_raw(ptr) });
        }
    }
}

impl TryFrom<String> for FfiString {
    type Error = Error;
    fn try_from(s: String) -> Result<Self> {
        let cstr = CString::new(s)?;
        Ok(cstr.into())
    }
}

impl<'a> TryFrom<&'a str> for FfiString {
    type Error = Error;

    fn try_from(s: &'a str) -> Result<Self> {
        FfiString::try_from(s.to_string())
    }
}

/// Allows conversion into `*const` pointers for types that hold data internal to `ldap_sys`
pub trait AsPtr {
    /// Type of the internal data
    type Pointer;

    /// Get a `*const` pointer of the internal data
    fn as_ptr(&self) -> *const Self::Pointer;
}

/// Allows conversion into `*mut` pointers for types that hold data internal to `ldap_sys`
pub trait AsMutPtr {
    /// Type of the internal data
    type Pointer;

    /// Get a `*mut` poiter of the internal data
    fn as_mut_ptr(&mut self) -> *mut Self::Pointer;
}

pub trait AsPtrVecExt<P: AsPtr> {
    fn as_ptrs(&self) -> Vec<*const P::Pointer>;

    fn as_ptrs_null(&self) -> Vec<*const P::Pointer> {
        let mut vs = self.as_ptrs();
        vs.push(std::ptr::null());
        vs
    }
}

impl<T> AsPtrVecExt<T> for Vec<T>
where
    T: AsPtr,
{
    fn as_ptrs(&self) -> Vec<*const T::Pointer> {
        self.iter().map(|v| v.as_ptr()).collect()
    }
}

pub trait AsMutPtrVecExt<P: AsMutPtr> {
    fn as_mut_ptrs(&mut self) -> Vec<*mut P::Pointer>;

    fn as_mut_ptrs_null(&mut self) -> Vec<*mut P::Pointer> {
        let mut vs = self.as_mut_ptrs();
        vs.push(std::ptr::null_mut());
        vs
    }
}

impl<T> AsMutPtrVecExt<T> for Vec<T>
where
    T: AsMutPtr,
{
    fn as_mut_ptrs(&mut self) -> Vec<*mut T::Pointer> {
        self.iter_mut().map(|v| v.as_mut_ptr()).collect()
    }
}
