//! LDAPper -- Straight forward `async` LDAP access
//!
//! LDAPper provides asynchronous access to LDAP by wrapping the C-Library (`libldap`)
//!
//! The main point of interaction is the [`Connection`] class and methods.
//!
//! # Status
//!
//! This library is still highly in development and not production ready.

use ldap_sys::*;

use std::collections::HashMap;
use std::convert::TryInto;
use std::ffi::{CStr, CString};
use std::future::Future;
use std::os::raw::{c_char, c_int, c_void};
use std::pin::Pin;
use std::sync::atomic::{AtomicPtr, Ordering};
use std::sync::{Arc, RwLock};
use std::task::{Context, Poll};

use tokio::stream::Stream;
use tokio::sync::mpsc::{channel, Receiver, Sender};
use tokio::task::JoinHandle;

mod error;
pub use error::Error;
mod helper;
use helper::*;

#[cfg(test)]
mod tests;

pub type Result<T> = std::result::Result<T, Error>;

/// Represents a connection to an LDAP server. This spawns a loop in
/// the background that is continuosly receiving messages and waking
/// the appropiate futures
pub struct Connection {
    state: Arc<ConnectionState>,
}

impl Connection {
    /// Connect to the LDAP server found at `addr`.
    ///
    /// This internally calls `ldap_initialize`, refer `ldap_init (3)`
    pub fn connect(addr: &str) -> Result<Self> {
        let addr: FfiString = addr.try_into()?;
        let mut ldap: *mut LDAP = std::ptr::null_mut();
        let result = unsafe { ldap_initialize(&mut ldap, addr.as_ptr()) };
        if result != LDAP_SUCCESS as c_int {
            // The result of `ldap` is undefined in this case
            return Err(Error::LdapError {
                errcode: result,
                msg: None,
            });
        }
        let con = Connection {
            state: Arc::new(ConnectionState {
                inner: AtomicPtr::new(ldap),
                registry: RwLock::new(HashMap::new()),
                inbox: RwLock::new(HashMap::new()),
                wait_thread: RwLock::new(None),
            }),
        };
        Ok(con)
    }

    /// Cleanup any inboxes that have not been claimed in the last 5 seconds. This should be called
    /// regulary to "garabe-collect" any orphaned messages
    pub async fn cleanup(&self) -> Result<()> {
        use std::time::SystemTime;

        let ts = SystemTime::now()
            .duration_since(SystemTime::UNIX_EPOCH)
            .unwrap()
            .as_secs();

        self.state.inbox.write()?.retain(|_k, v| {
            let v_ts = v
                .iter()
                .map(|m| m.as_ref().map(|msg| msg.ts).unwrap_or(0))
                .max();
            v_ts < Some(ts + 5)
        });

        Ok(())
    }

    /// Start a future from a function returning a ldap message id
    ///
    /// This internally constructs a [`Future`] that executes _fun_ first and waits for a single
    /// `Message` to be returned by polling [`ldap_result`]
    ///
    /// [`Future`]: std::future::Future
    /// [`ldap_result`]: ldap_sys::ldap_result
    pub fn start_future<F: FnOnce(*mut LDAP) -> Result<c_int>>(&self, fun: F) -> LdapFuture<F> {
        LdapFuture::from_fn(self, fun)
    }

    /// Start a stream from a function that returns a ldap message id
    ///
    /// This internally constructs a [`Stream`] that executes _fun_ first and waits for a chain of
    /// `Message`s to be returned by polling [`ldap_result`]
    ///
    /// [`Stream`]: tokio::stream::Stream
    /// [`ldap_result`]: ldap_sys::ldap_result
    pub fn start_stream<F: FnOnce(*mut LDAP) -> Result<c_int>>(&self, fun: F) -> LdapStream<F> {
        LdapStream::from_fn(self, fun)
    }

    /// Set the protocol version
    pub fn set_version(&self, version: LdapVersion) -> Result<()> {
        let ld = self.state.inner.load(Ordering::Relaxed);
        let mut version = version.as_ldap();
        let version_ref = &mut version as *mut _ as *mut c_void;

        let result = unsafe { ldap_set_option(ld, LDAP_OPT_PROTOCOL_VERSION as i32, version_ref) };

        if result == LDAP_OPT_SUCCESS as i32 {
            Ok(())
        } else {
            Err(Error::LdapError {
                errcode: result,
                msg: None,
            })
        }
    }

    /// Start a new `ldap_simple_bind` operation
    ///
    /// Please note that all strings must be ASCII-compatabile
    #[deprecated(note = "Deprecated in libldap. Use `ldap_sasl_bind` instead")]
    pub async fn simple_bind(&self, who: &str, passwd: &str) -> Result<Message> {
        let who_cstr: FfiString = CString::new(who)?.into();
        let passwd_cstr: FfiString = CString::new(passwd)?.into();

        self.start_future(|ld| unsafe {
            #[allow(deprecated)]
            Ok(ldap_simple_bind(
                ld,
                who_cstr.as_ptr(),
                passwd_cstr.as_ptr(),
            ))
        })
        .await
    }

    /// Unbind the current connection
    pub fn unbind(&self) -> Result<()> {
        let ld = self.state.inner.load(Ordering::Relaxed);
        let res = unsafe { ldap_unbind_ext(ld, std::ptr::null_mut(), std::ptr::null_mut()) };
        if res == LDAP_SUCCESS as i32 {
            Ok(())
        } else {
            Err(Error::LdapError {
                errcode: res,
                msg: None,
            })
        }
    }

    /// Perform a search operation
    ///
    /// Please note that all strings must be ASCII-compatabile
    ///
    /// This internally calls [`ldap_sys::ldap_search_ext`]
    ///
    /// See also: `ldap_search (3)`
    pub fn search(
        &self,
        base: String,
        scope: SearchScope,
        filter: String,
        attrs: Vec<String>,
        sizelimit: i32,
    ) -> impl Stream<Item = Result<Message>> {
        self.search_ext(base, scope, filter, attrs, vec![], vec![], sizelimit)
    }

    /// Perform an extended search operation
    ///
    /// Please note that any strings *must* be ASCII-compatabile
    ///
    /// This internally makes a call to [`ldap_sys::ldap_search_ext`]
    ///
    /// See also: `ldap_search (3)`
    #[allow(clippy::too_many_arguments)]
    pub fn search_ext(
        &self,
        base: String,
        scope: SearchScope,
        filter: String,
        attrs: Vec<String>,
        mut serverctrls: Vec<Control>,
        mut clientctrls: Vec<Control>,
        sizelimit: i32,
    ) -> impl Stream<Item = Result<Message>> {
        self.start_stream(move |ld| {
            // prepare variables for crossing the border.. note that
            // instead of a pointer to an empty array, the api expects
            // a null-pointer directly.
            let base_cstr: FfiString = CString::new(base)?.into();
            let filter_cstr: FfiString = CString::new(filter)?.into();
            let scope = scope.to_ldap();
            let mut attrs: Vec<FfiString> =
                attrs.into_iter().map(|s| s.try_into().unwrap()).collect();
            let mut attrsl = attrs.as_mut_ptrs_null();
            let attrsp = if attrsl.len() == 1 {
                std::ptr::null_mut()
            } else {
                attrsl.as_mut_ptr()
            };
            let mut sctrls = serverctrls.as_mut_ptrs_null();
            let sctrlsp = if sctrls.len() == 1 {
                std::ptr::null_mut()
            } else {
                sctrls.as_mut_ptr()
            };
            let mut cctrls = clientctrls.as_mut_ptrs_null();
            let cctrlsp = if cctrls.len() == 1 {
                std::ptr::null_mut()
            } else {
                cctrls.as_mut_ptr()
            };
            let mut msgid: c_int = 0;

            let res = unsafe {
                ldap_search_ext(
                    ld,
                    base_cstr.as_ptr(),
                    scope,
                    filter_cstr.as_ptr(),
                    attrsp,
                    0, // attrsonly
                    sctrlsp,
                    cctrlsp,
                    std::ptr::null_mut(), // timeout
                    sizelimit,
                    &mut msgid,
                )
            };

            if res == LDAP_SUCCESS as i32 {
                Ok(msgid)
            } else {
                Err(Error::LdapError {
                    errcode: res,
                    msg: None,
                })
            }
        })
    }
}

/// The internal state of a connection
struct ConnectionState {
    /// The pointer to the FFI `LDAP` object
    inner: AtomicPtr<LDAP>,
    /// Registry of any known running `Future`s that await a response
    registry: RwLock<HashMap<c_int, SharedTaskState>>,
    /// An 'Inbox' that serves as temporary storage for `Message`s without a known `Future`, in case
    /// we receive a response for a `Future` that is yet to be registered.
    inbox: RwLock<HashMap<c_int, Vec<Result<Message>>>>,
    /// Handle to the thread that is actively polling `ldap_result`
    wait_thread: RwLock<Option<JoinHandle<()>>>,
}

impl ConnectionState {
    /// Function that gets executed as wait thread. Continuosly polls `ldap_result` and handles the
    /// results. Self-Terminates if there are no other living references to the `ConnectionState`
    fn wait_thread_worker(self: Arc<Self>) {
        // Timeout to use. We don't wait indefinetly so we can stop the thread once there are no
        // running `Futures` nor any living handle to the connection state other than our own
        let mut timeval = ldap_sys::timeval {
            tv_sec: 1,
            tv_usec: 0,
        };
        'l: loop {
            let ld = self.inner.load(Ordering::Relaxed);
            let mut msg: *mut ldap_sys::LDAPMessage = std::ptr::null_mut();

            // Query for any new messages.
            // Don't query for `all`, since we prefer them one-by-one
            let result = unsafe {
                ldap_result(
                    ld,
                    LDAP_RES_ANY, // msgid
                    0,            // all
                    &mut timeval,
                    &mut msg,
                )
            };

            let msgs = match result {
                // result > 0 indicates it contains valid data, the result is the message type
                r if r > 0 => ConnectionState::parse_result(self.clone(), msg),
                // result == 0 indicates a timeout
                r if r == 0 => {
                    let ct = Arc::strong_count(&self);
                    if ct == 1 {
                        // Since we have the only reference to the ConnectionState that means, that
                        // no Connection-Object is still alive. Therefore there can be no more new
                        // request and we can savely kill the loop
                        break 'l;
                    } else {
                        Err(Error::Timeout)
                    }
                }
                // result < 0 indicates an error
                r if r < 0 => {
                    eprintln!("ERROR IN LDAP_RESLT: {}", r);
                    panic!(r);
                }
                // we covered all our bases
                _ => unreachable!(),
            };

            match msgs {
                Ok(m) => ConnectionState::on_message_chain(&self, m),
                // Ignore timeouts, we would've exited at this point already if we needed to
                Err(Error::Timeout) => Ok(()),
                Err(e) => ConnectionState::on_err_result(&self, unsafe { ldap_msgid(msg) }, e),
            }
            .unwrap();

            // properly free the data if any exists
            if !msg.is_null() {
                unsafe {
                    ldap_msgfree(msg);
                }
            }
        }
    }

    fn parse_result(self: Arc<Self>, msg: *mut LDAPMessage) -> Result<Vec<Message>> {
        let ld = self.inner.load(Ordering::Relaxed);
        let cnt = unsafe { ldap_count_messages(ld, msg) } as usize;
        let mut msgs = Vec::with_capacity(cnt);

        // collect the message chain
        unsafe {
            let mut current = ldap_first_message(ld, msg);
            while !current.is_null() {
                let m = Message::from_ldap(ld, current)?;
                msgs.push(m);
                current = ldap_next_message(ld, current);
            }
        }

        Ok(msgs)
    }

    /// Parse a valid message chain received
    fn on_message_chain(self: &Arc<Self>, msg: Vec<Message>) -> Result<()> {
        // Get the id of this message chain
        let msg_id = msg[0].msg_id;
        // if any of the messages is a final one, we can remove the
        // msg_id from the registry afterwards
        let remove_from_registry = msg.iter().any(|m| m.is_final());

        if let Some(ref mut state) = self.registry.write()?.get_mut(&msg_id) {
            // The msg_id was previously registered, so we sent them through that channel.
            let mut tx = state.tx.clone();
            tokio::task::spawn(async move {
                for m in msg.into_iter() {
                    // TODO: Remove from registry if we get an error here
                    tx.send(Ok(m)).await.ok();
                    if remove_from_registry {
                        tx.send(Err(Error::NoMessage)).await.ok();
                    }
                }
            });
        } else {
            // If we don't know about the msg_id yet, we'll save them in the inbox for now.
            let mut msg_results = msg.into_iter().map(Result::Ok).collect();
            let mut inbox_lock = self.inbox.write()?;
            let inbox = match inbox_lock.get_mut(&msg_id) {
                Some(i) => i,
                None => {
                    inbox_lock.insert(msg_id, Vec::new());
                    inbox_lock.get_mut(&msg_id).unwrap()
                }
            };
            inbox.append(&mut msg_results);
        };

        if remove_from_registry {
            self.registry.write()?.remove(&msg_id);
        }

        Ok(())
    }

    /// Handle an errenous result from the waiter thread
    fn on_err_result(self: &Arc<Self>, msg_id: c_int, err: Error) -> Result<()> {
        if let Some(ref mut state) = self.registry.write()?.get_mut(&msg_id) {
            // The msg_id was previously registered, so we sent them through that channel.
            let mut tx = state.tx.clone();
            tokio::task::spawn(async move {
                tx.send(Err(err)).await.ok();
            });
        } else {
            // If we don't know about the msg_id yet, we'll save them in the inbox for now.
            let mut inbox_lock = self.inbox.write()?;
            let inbox = match inbox_lock.get_mut(&msg_id) {
                Some(i) => i,
                None => {
                    inbox_lock.insert(msg_id, Vec::new());
                    inbox_lock.get_mut(&msg_id).unwrap()
                }
            };
            inbox.push(Err(err));
        };
        Ok(())
    }

    /// Register a `Future` or `Stream`. Sends any stored messages
    fn register_task(self: &Arc<Self>, task_state: SharedTaskState) -> Result<()> {
        self.start_wait_thread()?;
        if let Some(inbox) = self.inbox.write()?.remove(&task_state.msg_id) {
            let mut state = task_state.clone();
            tokio::spawn(async move {
                for msg in inbox.into_iter() {
                    // TODO: Handle error.
                    state.tx.send(msg).await.ok();
                }
            });
        }
        self.registry.write()?.insert(task_state.msg_id, task_state);

        Ok(())
    }

    /// Start a new thread as waiter thread if none is running. Executes `wait_thread_worker`
    fn start_wait_thread(self: &Arc<Self>) -> Result<()> {
        if self.wait_thread.read()?.is_none() {
            let mut lock = self.wait_thread.write()?;
            if lock.is_none() {
                let state = self.clone();
                *lock = Some(tokio::task::spawn_blocking(move || {
                    state.wait_thread_worker()
                }));
            }
        }

        Ok(())
    }
}

/// The state shared between the running thread and the task
#[derive(Clone)]
pub struct SharedTaskState {
    /// Message-ID returned by the LDAP function
    msg_id: c_int,
    /// Handle to the `Sender` to the `Future`
    tx: Sender<Result<Message>>,
}

/// A single message from LDAP, may contain one or more [`Entry`]
#[derive(Debug)]
pub struct Message {
    /// The LDAP-internal id of this message
    pub msg_id: c_int,
    pub matched_dn: String,
    /// The type of result this message represents
    pub ty: ResultType,
    pub entries: Vec<Entry>,
    /// Seconds since the UNIX Epoch at the time of creation
    pub ts: u64,
}

impl Message {
    pub(crate) unsafe fn from_ldap(ld: *mut LDAP, msg: *mut LDAPMessage) -> Result<Self> {
        use std::time::SystemTime;

        let mut errcode: c_int = 0;
        let mut matched_dn: *mut c_char = std::ptr::null_mut();
        let mut errmsg: *mut c_char = std::ptr::null_mut();
        let mut serverctrls: *mut *mut LDAPControl = std::ptr::null_mut();

        let result = ldap_parse_result(
            ld,
            msg,
            &mut errcode,
            &mut matched_dn,
            &mut errmsg,
            std::ptr::null_mut(), // referralsp
            &mut serverctrls,
            0, // int freeit
        );

        if result != LDAP_SUCCESS as i32 {
            return Err(Error::LdapError {
                errcode: result,
                msg: None,
            });
        };

        let mut errmsg = ForeignMutPtr::new(errmsg, _ldap_memfree);
        let mut matched_dn = ForeignMutPtr::new(matched_dn, _ldap_memfree);
        let _sctrls = ForeignMutPtr::new(serverctrls, |p| ldap_controls_free(p));

        let msg_ty = match errcode {
            _ if errcode > 0 => errcode,
            _ if errcode == 0 => {
                return Err(Error::Timeout);
            }
            x if errcode < 0 => {
                let msg = if !errmsg.as_ptr().is_null() {
                    let cs = CStr::from_ptr(errmsg.as_mut_ptr());
                    Some(cs.to_str().unwrap().to_owned())
                } else {
                    None
                };
                return Err(Error::LdapError { errcode: x, msg });
            }
            _ => unreachable!(), // we covered all cases
        };

        let matched_dn = CStr::from_ptr(matched_dn.as_mut_ptr())
            .to_str()
            .unwrap()
            .to_owned();
        let ts = SystemTime::now()
            .duration_since(SystemTime::UNIX_EPOCH)
            .unwrap()
            .as_secs();

        let msg_id = ldap_msgid(msg);
        let ty = ResultType::from_ldap(msg_ty).unwrap();

        let entry_cnt = ldap_count_entries(ld, msg);
        let mut entries: Vec<Entry> = Vec::with_capacity(entry_cnt as usize);

        let mut current = ldap_first_entry(ld, msg);
        while !current.is_null() {
            let entry = Entry::from_ldap(ld, current)?;
            entries.push(entry);
            current = ldap_next_entry(ld, current);
        }

        Ok(Message {
            msg_id,
            matched_dn,
            ty,
            ts,
            entries,
        })
    }

    fn is_final(&self) -> bool {
        match self.ty {
            ResultType::SearchEntry | ResultType::SearchReference => false,

            ResultType::Bind
            | ResultType::SearchResult
            | ResultType::Modify
            | ResultType::Add
            | ResultType::Delete
            | ResultType::ModDN
            | ResultType::Compare
            | ResultType::Extended
            | ResultType::Intermediate
            | ResultType::Other(_) => true,
        }
    }

    pub fn get_type(&self) -> ResultType {
        self.ty
    }
}

/// The type of a [`Message`]
#[derive(Debug, Clone, Copy)]
pub enum ResultType {
    Bind,
    SearchEntry,
    SearchReference,
    SearchResult,
    Modify,
    Add,
    Delete,
    ModDN,
    Compare,
    Extended,
    Intermediate,

    /// Other result type not found
    Other(u64),
}

impl ResultType {
    pub fn from_ldap(code: c_int) -> Option<Self> {
        match code as u64 {
            LDAP_RES_BIND => Some(ResultType::Bind),
            LDAP_RES_SEARCH_ENTRY => Some(ResultType::SearchEntry),
            LDAP_RES_SEARCH_REFERENCE => Some(ResultType::SearchReference),
            LDAP_RES_SEARCH_RESULT => Some(ResultType::SearchResult),
            LDAP_RES_MODIFY => Some(ResultType::Modify),
            LDAP_RES_ADD => Some(ResultType::Add),
            LDAP_RES_DELETE => Some(ResultType::Delete),
            LDAP_RES_MODDN => Some(ResultType::ModDN),
            LDAP_RES_COMPARE => Some(ResultType::Compare),
            LDAP_RES_EXTENDED => Some(ResultType::Extended),
            LDAP_RES_INTERMEDIATE => Some(ResultType::Intermediate),
            _ => None,
        }
    }
}

/// A single entry of an object; Contains a list of Attributes with zero to N [`Value`]s
#[derive(Debug)]
pub struct Entry {
    pub attributes: HashMap<String, Vec<Value>>,
}

impl Entry {
    pub(crate) unsafe fn from_ldap(ld: *mut LDAP, msg: *mut LDAPMessage) -> Result<Self> {
        let mut attributes = HashMap::new();
        let mut ber: *mut BerElement = std::ptr::null_mut();
        let mut name: *mut c_char;

        name = ldap_first_attribute(ld, msg, &mut ber);
        while !name.is_null() {
            let c_name = std::ffi::CStr::from_ptr(name);
            // XXX: Not sure if we can assume that all attributes have pure-ASCII names.
            let r_name = c_name.to_str()?;
            attributes.insert(r_name.to_owned(), Vec::new());

            let values = ldap_get_values_len(ld, msg, name);
            let val_ct = ldap_count_values_len(values) as usize;

            let vals = std::slice::from_raw_parts(values, val_ct);
            for v in vals {
                let v = Value::from_ldap(*v);
                // we can unwrap here since we added the entry previously
                attributes.get_mut(r_name).unwrap().push(v);
            }

            ldap_value_free_len(values);
            ldap_memfree(name as *mut std::ffi::c_void);
            name = ldap_next_attribute(ld, msg, ber);
        }

        Ok(Entry { attributes })
    }
}

/// A Value returned from LDAP. These are usually binary data, which
/// might not be parsable as text. However, all text is formatted as
/// UTF-8, starting with protocol version 3. (see:
/// [`Connection::set_version`])
#[derive(Debug)]
pub struct Value {
    data: Vec<i8>,
}

impl Value {
    /// Create a new Value struct by parsing the unsafe data provided by `ldap-sys`
    pub(crate) unsafe fn from_ldap(val: *mut BerValue) -> Self {
        let len = (*val).bv_len as usize;
        let data = std::slice::from_raw_parts((*val).bv_val, len);
        let mut v = vec![0; len];
        v.clone_from_slice(data);

        Value { data: v }
    }

    pub fn new(data: &[i8]) -> Self {
        Value {
            data: Vec::from(data),
        }
    }

    pub fn empty() -> Self {
        Self::new(&Vec::new()[..])
    }

    pub fn get_data(&self) -> &[i8] {
        &self.data[..]
    }

    pub fn get_data_mut(&mut self) -> &mut [i8] {
        &mut self.data[..]
    }

    pub(crate) fn to_berval(&self) -> _BerValue {
        _BerValue::new(&self.data[..])
    }
}

/// Wrapper around BerValue so we still hold the data and dont have to
/// worry about accidentally dropping it before the pointer to it.
struct _BerValue {
    _data: Vec<i8>,
    inner: BerValue,
}

impl _BerValue {
    pub fn new(data: &[i8]) -> Self {
        let mut data = Vec::from(data);
        let bv = BerValue {
            bv_len: data.len() as u64,
            bv_val: data.as_mut_ptr(),
        };
        _BerValue {
            inner: bv,
            _data: data,
        }
    }
}

impl AsPtr for _BerValue {
    type Pointer = BerValue;

    fn as_ptr(&self) -> *const Self::Pointer {
        &self.inner
    }
}

impl AsMutPtr for _BerValue {
    type Pointer = <Self as AsPtr>::Pointer;

    fn as_mut_ptr(&mut self) -> *mut Self::Pointer {
        &mut self.inner
    }
}

/// Wrapper around `LDAPMod`
pub struct Mod {
    op: c_int,
    ty: FfiString,
    values: Vec<_BerValue>,
}

impl Mod {
    pub fn new_add(attr: String) -> Result<Self> {
        let ty: FfiString = attr.try_into()?;
        let values = Vec::new();

        Ok(Mod {
            op: (LDAP_MOD_ADD | LDAP_MOD_BVALUES) as i32,
            ty,
            values,
        })
    }
}

/// A wrapper for `LDAPControl`
pub struct Control {
    inner: *mut LDAPControl,
}

impl Control {
    pub fn create(oid: &str, critical: bool, value: &Value, dupval: bool) -> Result<Self> {
        let oid_cstr: FfiString = oid.try_into()?;
        let mut value_bval = value.to_berval();
        let iscritical: c_int = if critical { 1 } else { 0 };
        let dupval: c_int = if dupval { 1 } else { 0 };
        let mut val: *mut LDAPControl = std::ptr::null_mut();

        let result = unsafe {
            ldap_control_create(
                oid_cstr.as_ptr(),
                iscritical,
                value_bval.as_mut_ptr(),
                dupval,
                &mut val,
            )
        };

        if result == LDAP_SUCCESS as i32 {
            Ok(Control { inner: val })
        } else {
            Err(Error::LdapError {
                errcode: result,
                msg: None,
            })
        }
    }

    // TODO: functions for all the other controls, including values if need be

    pub fn sort_request() -> Result<Self> {
        let oid = String::from_utf8(LDAP_CONTROL_SORTREQUEST.to_vec())?;
        Self::create(&oid, false, &Value::empty(), false)
    }
}

impl AsPtr for Control {
    type Pointer = LDAPControl;

    fn as_ptr(&self) -> *const Self::Pointer {
        self.inner
    }
}

impl AsMutPtr for Control {
    type Pointer = <Self as AsPtr>::Pointer;

    fn as_mut_ptr(&mut self) -> *mut Self::Pointer {
        self.inner
    }
}

impl std::ops::Drop for Control {
    fn drop(&mut self) {
        unsafe {
            ldap_control_free(self.inner);
        }
    }
}

/// A usable protocol version. Used with [`Connection::set_version`]
pub enum LdapVersion {
    /// Protocol version 2. Equivalent to `LDAP_VERSION2`
    V2,
    /// Protocol version 3. Equivalent to `LDAP_VERSION3`
    V3,
}

impl LdapVersion {
    pub fn from_ldap(code: u32) -> Self {
        match code {
            LDAP_VERSION2 => LdapVersion::V2,
            LDAP_VERSION3 => LdapVersion::V3,
            _ => panic!("Unsupported LDAP Version"),
        }
    }

    pub fn as_ldap(&self) -> u32 {
        match self {
            LdapVersion::V2 => LDAP_VERSION2,
            LdapVersion::V3 => LDAP_VERSION3,
        }
    }
}

/// Defines the scope of a search for use with [`Connection::search`], [`Connection::search_ext`], and
/// friends
///
/// [`Connection.search_ext`]: crate::Connection#method.search_ext
pub enum SearchScope {
    /// Searches only the object specified
    Base,
    /// Search only the objects immediate children
    OneLevel,
    /// Search the object and all its descendants
    Subtree,
    /// Search all the descendants
    Children,
    Default,
    /// An unknown/undefined scope value
    Unknown(ber_int_t),
}

impl SearchScope {
    /// Convert from `LDAP_SCOPE_*` value
    pub fn from_ldap(code: ber_int_t) -> Self {
        match code {
            LDAP_SCOPE_BASE => SearchScope::Base,
            LDAP_SCOPE_ONELEVEL => SearchScope::OneLevel,
            LDAP_SCOPE_SUBTREE => SearchScope::Subtree,
            LDAP_SCOPE_CHILDREN => SearchScope::Children,
            LDAP_SCOPE_DEFAULT => SearchScope::Default,
            other => SearchScope::Unknown(other),
        }
    }

    /// Convert into `LDAP_SCOPE_*` value
    pub fn to_ldap(&self) -> ber_int_t {
        match self {
            SearchScope::Base => LDAP_SCOPE_BASE,
            SearchScope::OneLevel => LDAP_SCOPE_ONELEVEL,
            SearchScope::Subtree => LDAP_SCOPE_SUBTREE,
            SearchScope::Children => LDAP_SCOPE_CHILDREN,
            SearchScope::Default => LDAP_SCOPE_DEFAULT,
            SearchScope::Unknown(other) => *other,
        }
    }
}

/// Abstracts an asynchronous LDAP function call that receives exactly one [`Message`] as response
///
/// # Invariants
///
/// Exactly one of `msg_id` and `fun` is None
///
/// # Notes
///
/// While after registering still holding the `ConnectionState` is strictly speaking not neccessary
/// it prohibits the waiter-thread from exiting before the `Future` is fully resolved
pub struct LdapFuture<F: FnOnce(*mut LDAP) -> Result<c_int>> {
    msg_id: Option<c_int>,
    fun: Option<Box<F>>,
    rx: Receiver<Result<Message>>,
    tx: Sender<Result<Message>>,
    con: Arc<ConnectionState>,
    registered: bool,
}

impl<F> LdapFuture<F>
where
    F: FnOnce(*mut LDAP) -> Result<c_int>,
{
    /// Capacity of the internal channel
    ///
    /// Since a `LdapFuture` waits for only a single [`Message`] we only need a capacity of 1. To call
    /// a function that expects more than a single [`Message`], [`LdapStream`] should be used instead
    pub const CHANNEL_CAPACITY: usize = 1;

    /// Construct from an existing message id
    ///
    /// [`register_task`]: crate::ConnectionState::register_task
    pub fn from_msg_id(con: &Connection, msg_id: c_int) -> Result<Self> {
        let (tx, rx) = channel(Self::CHANNEL_CAPACITY);

        Ok(LdapFuture {
            msg_id: Some(msg_id),
            fun: None,
            con: con.state.clone(),
            rx,
            tx,
            registered: false,
        })
    }

    /// Construct from a closure that returns a message id
    ///
    /// The closure will be executed on the first poll and registered with the `ConnectionState`
    pub(crate) fn from_fn(con: &Connection, fun: F) -> Self {
        let (tx, rx) = channel(Self::CHANNEL_CAPACITY);

        LdapFuture {
            msg_id: None,
            fun: Some(Box::new(fun)),
            con: con.state.clone(),
            rx,
            tx,
            registered: false,
        }
    }

    /// Get a pinned reference to the `Receiver` listening to [`Message`]s
    pub(crate) fn get_pin_mut_rx(self: Pin<&mut Self>) -> Pin<&mut Receiver<Result<Message>>> {
        unsafe { self.map_unchecked_mut(|s| &mut s.rx) }
    }

    fn register(&mut self, msg_id: c_int) -> Result<()> {
        if !self.registered {
            self.con.register_task(SharedTaskState {
                tx: self.tx.clone(),
                msg_id,
            })?;
        }
        self.registered = true;
        Ok(())
    }

    fn poll_rx(self: Pin<&mut Self>, ctx: &mut Context) -> Poll<<Self as Future>::Output> {
        let rx = self.get_pin_mut_rx();
        match rx.poll_next(ctx) {
            Poll::Ready(Some(v)) => Poll::Ready(v),
            Poll::Ready(None) => Poll::Ready(Err(Error::NoMessage)),
            Poll::Pending => Poll::Pending,
        }
    }
}

impl<F> Future for LdapFuture<F>
where
    F: FnOnce(*mut LDAP) -> Result<c_int>,
{
    type Output = Result<Message>;

    fn poll(self: Pin<&mut Self>, ctx: &mut Context) -> Poll<Self::Output> {
        let mut s = self;
        match s.msg_id {
            Some(msg_id) => {
                // Future is already being executed, we poll on our channel instead
                let this = s.get_mut();
                this.register(msg_id)?;
                s = Pin::new(this);
            }
            None => {
                // Let's get this future started
                let this = s.get_mut();
                let fun = this.fun.take().unwrap();
                let ld = this.con.inner.load(Ordering::Relaxed);
                let msg_id = fun(ld)?;
                this.msg_id = Some(msg_id);
                this.register(msg_id)?;
                s = Pin::new(this);
            }
        }

        s.poll_rx(ctx)
    }
}

/// Abstracts an asynchronous LDAP function call that receives more than a single message as
/// response. Internally uses a channel as communcation device from the worker wait thread run by
/// the [`Connection`]
///
/// # Invariants
/// Exactly one of `msg_id` or `fun` is `None`
pub struct LdapStream<F: FnOnce(*mut LDAP) -> Result<c_int>> {
    msg_id: Option<c_int>,
    fun: Option<Box<F>>,
    rx: Receiver<Result<Message>>,
    tx: Sender<Result<Message>>,
    con: Arc<ConnectionState>,
    registered: bool,
}

impl<F> LdapStream<F>
where
    F: FnOnce(*mut LDAP) -> Result<c_int>,
{
    // Default capacity of the internal channel
    pub const CHANNEL_CAPACITY: usize = 100;

    /// Constructs a stream from an existing message id.
    pub fn from_msg_id(con: &Connection, msg_id: c_int) -> Self {
        Self::from_msg_id_with_capacity(con, msg_id, Self::CHANNEL_CAPACITY)
    }

    /// Like [`LdapStream::from_msg_id`] but with defined capacity of the backing channel
    pub fn from_msg_id_with_capacity(con: &Connection, msg_id: c_int, capacity: usize) -> Self {
        let (tx, rx) = channel(capacity);

        LdapStream {
            msg_id: Some(msg_id),
            fun: None,
            con: con.state.clone(),
            rx,
            tx,
            registered: false,
        }
    }

    /// Construct from a closure returning a ldap message id
    pub fn from_fn(con: &Connection, fun: F) -> Self {
        Self::from_fn_with_capacity(con, fun, Self::CHANNEL_CAPACITY)
    }

    /// Like [`LdapStream::from_fn`] but with defined capacity of the backing channel
    pub fn from_fn_with_capacity(con: &Connection, fun: F, capacity: usize) -> Self {
        let (tx, rx) = channel(capacity);

        LdapStream {
            msg_id: None,
            fun: Some(Box::new(fun)),
            con: con.state.clone(),
            rx,
            tx,
            registered: false,
        }
    }

    /// Project to the `Receiver` of the backing channel
    pub(crate) fn get_pin_mut_rx(self: Pin<&mut Self>) -> Pin<&mut Receiver<Result<Message>>> {
        unsafe { self.map_unchecked_mut(|s| &mut s.rx) }
    }

    /// Register this stream with the given _msg_id_
    fn register(&mut self, msg_id: c_int) -> Result<()> {
        if !self.registered {
            self.con.register_task(SharedTaskState {
                tx: self.tx.clone(),
                msg_id,
            })?;
        }
        self.registered = true;
        Ok(())
    }

    /// Poll the inner `Receiver` and map the result to the appropiate format
    fn poll_rx(self: Pin<&mut Self>, ctx: &mut Context) -> Poll<Option<<Self as Stream>::Item>> {
        let rx = self.get_pin_mut_rx();
        match rx.poll_next(ctx) {
            Poll::Ready(Some(Err(Error::NoMessage))) => Poll::Ready(None),
            other => other,
        }
    }
}

impl<F> Stream for LdapStream<F>
where
    F: FnOnce(*mut LDAP) -> Result<c_int>,
{
    type Item = Result<Message>;

    fn poll_next(
        self: std::pin::Pin<&mut Self>,
        ctx: &mut std::task::Context<'_>,
    ) -> Poll<Option<Self::Item>> {
        let mut s = self;
        if let Some(msg_id) = s.msg_id {
            // Stream already started.
            let this = s.get_mut();
            this.register(msg_id)?;
            s = Pin::new(this);
        } else {
            // we need to start the stream etc
            let this = s.get_mut();
            let ld = this.con.inner.load(Ordering::Relaxed);
            let fun = this.fun.take().unwrap();
            let msg_id = fun(ld)?;
            this.msg_id = Some(msg_id);
            this.register(msg_id)?;
            s = Pin::new(this);
        }

        s.poll_rx(ctx)
    }
}
